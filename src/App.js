import './App.css';
import {useEffect, useState} from 'react';
import image from "./mountain.jpeg";
import image2 from './image2.png';

function App() {

  const [theme,setTheme] = useState("dark");


  const HandleChange = (e) => {
    setTheme(e.target.value);
  }

  useEffect(()=>{
    
    console.log(theme);
    let elements = document.getElementsByClassName("theme-picker");
    let themeList = ['light', 'dark', 'contrast'];
    let themeListToBeRemoved = [];
    for (let i=0;i<themeList.length;i++){
      // console.log(iter,theme)
      if( themeList[i] != theme){

        themeListToBeRemoved.push(themeList[i]);
      }
    }
    
    for(let i=0;i<elements.length;i++){
      elements[i].classList.add(theme);

      for(let j=0;j<themeListToBeRemoved.length;j++){
        elements[i].classList.remove(themeListToBeRemoved[j]);
      }

    }

     
  });

  return (

      
    <div className='container'>
      <div className='theme-selector'>
        <button value="light"  onClick={HandleChange}> Light </button>
        <button value="dark" onClick={HandleChange} > Dark </button>
        <button value="contrast"  onClick={HandleChange}> Contrast </button>
      </div>
        
      <div className='box'>
          <img src={image2} alt="registration" style={{width:'50%'}}/>
          <div className='form-container'>
            
          <h1 className='heading' >Register here</h1>
            <form className='form'>
              <label className='label'>
                <div>First Name </div>
                <input type="text" name="firstName" className='input-box theme-picker' placeholder='Enter First Name Here'/>
              </label>
              <label className='label'>
                <div> Last Name </div>
                <input type="text" name="lastName" className='input-box theme-picker' placeholder='Enter Last Name Here'/>
              </label>
              <label className='label '>
                <div>Gender </div> 
                <input type="radio" value="Male" name="gender" className='input-radio theme-picker'/> Male
                <input type="radio" value="Female" name="gender" className='input-radio theme-picker'/> Female
                <input type="radio" value="Other" name="gender" className='input-radio theme-picker'/> Other
              </label>
              
              <label className='label'>
                <div>Password</div>
                <input type="password" name="password" className='input-box theme-picker' placeholder='Enter Password Here'/>
              </label>
              <label className='label'>
                <div>Email</div>
                <input type="email" name="email" className='input-box theme-picker'placeholder='Enter Email Here'/>
              </label>
              <input type="checkbox" name="checkbox" className='checkbox theme-picker'/>
              <label style={{margin:'10px'}}>I agree to the terms and conditions</label>

              <button type='submit' className='button'>
                Sign Up
              </button>
              
              <h4>Log in with</h4>
              <div>
              <svg className='icons theme-picker' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path d="M386.061 228.496c1.834 9.692 3.143 19.384 3.143 31.956C389.204 370.205 315.599 448 204.8 448c-106.084 0-192-85.915-192-192s85.916-192 192-192c51.864 0 95.083 18.859 128.611 50.292l-52.126 50.03c-14.145-13.621-39.028-29.599-76.485-29.599-65.484 0-118.92 54.221-118.92 121.277 0 67.056 53.436 121.277 118.92 121.277 75.961 0 104.513-54.745 108.965-82.773H204.8v-66.009h181.261zm185.406 6.437V179.2h-56.001v55.733h-55.733v56.001h55.733v55.733h56.001v-55.733H627.2v-56.001h-55.733z"/></svg>
              <svg className='icons theme-picker' xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M504 256C504 119 393 8 256 8S8 119 8 256c0 123.78 90.69 226.38 209.25 245V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.28c-30.8 0-40.41 19.12-40.41 38.73V256h68.78l-11 71.69h-57.78V501C413.31 482.38 504 379.78 504 256z"/></svg>
              </div>
           
            </form>
          </div>

      </div>
      
      <div className='table theme-picker'>

            <h1 className='tableHeading'>Table</h1>
              <table>
                <tr>
                  <th>Sport</th>
                  <th>Name</th>
                  <th>Country</th>
                </tr>
                <tr>
                  <td>Football</td>
                  <td>Lionel Messi</td>
                  <td>Argentina</td>
                </tr>
                <tr>
                  <td>Tennis </td>
                  <td>Roger Federer</td>
                  <td>Switzerland</td>
                </tr>
                <tr>
                  <td>Boxing </td>
                  <td>Muhammad Ali</td>
                  <td>USA</td>
                </tr>
              </table>
        
      </div>
           
      <div className="card theme-picker">
          <img src={image} alt="Mountain " style={{width: "100%"}} />
          <h1>Card</h1>
          
      </div>
          
    </div>
  );
}

export default App;
